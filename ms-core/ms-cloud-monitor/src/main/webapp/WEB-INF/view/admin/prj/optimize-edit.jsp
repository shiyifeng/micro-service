<%@page import="com.module.admin.prj.enums.PrjInfoContainer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑系统优化规则</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="id" value="${prjOptimize.id}">
		<div class="form-group">
			<label for="prjId" class="col-sm-4">项目</label>
			<div class="col-sm-8"><my:select id="prjId" headerKey="" headerValue="请选择项目" items="${prjInfos}" value="${prjOptimize.prjId}" cssCls="form-control" /></div>
		</div>
  		<div class="form-group">
			<label for="url" class="col-sm-4">地址 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="url" placeholder="接口的地址" value="${prjOptimize.url}"></div>
		</div>
  		<div class="form-group">
			<label for="warnTime" class="col-sm-4">预警时间 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="warnTime" placeholder="预警时间[单位ms]" value="${prjOptimize.warnTime}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	var info = {
	};
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			_saveMsg.attr('class', 'label label-danger');
			
			var id = $('#id').val();
			var prjId = $('#prjId');
			if(JUtil.isEmpty(prjId.val())) {
				_saveMsg.append('请选择项目');
				prjId.focus();
				return;
			}
			var url = $('#url');
			if(JUtil.isEmpty(url.val())) {
				_saveMsg.append('请输入接口地址');
				url.focus();
				return;
			}
			var warnTime = $('#warnTime');
			if(JUtil.isEmpty(warnTime.val())) {
				_saveMsg.append('请输入预警时间');
				warnTime.focus();
				return;
			}
			
			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/prjOptimize/f-json/save.shtml',
				data : {
					id: id,
					prjId: prjId.val(),
					url: url.val(),
					warnTime: warnTime.val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>