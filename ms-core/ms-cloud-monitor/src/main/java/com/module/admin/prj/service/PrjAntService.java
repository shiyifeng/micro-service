package com.module.admin.prj.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjAnt;
import com.system.handle.model.ResponseFrame;

/**
 * prj_ant的Service
 * @author autoCode
 * @date 2018-01-30 20:32:37
 * @version V1.0.0
 */
@Component
public interface PrjAntService {
	
	/**
	 * 保存或修改
	 * @param prjAnt
	 * @return
	 */
	public ResponseFrame saveOrUpdate(PrjAnt prjAnt);
	
	/**
	 * 根据paId获取对象
	 * @param paId
	 * @return
	 */
	public PrjAnt get(Integer paId);

	/**
	 * 分页获取对象
	 * @param prjAnt
	 * @return
	 */
	public ResponseFrame pageQuery(PrjAnt prjAnt);
	
	/**
	 * 根据paId删除对象
	 * @param paId
	 * @return
	 */
	public ResponseFrame delete(Integer paId);
}