package com.module.admin.prj.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.module.admin.prj.pojo.PrjApiTestDtl;

/**
 * prj_api_test_dtl的Dao
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
public interface PrjApiTestDtlDao {

	public abstract void save(PrjApiTestDtl prjApiTestDtl);

	public abstract void update(PrjApiTestDtl prjApiTestDtl);

	public abstract void delete(@Param("patId")Integer patId, @Param("path")String path);

	public abstract PrjApiTestDtl get(@Param("patId")Integer patId, @Param("path")String path);

	public abstract List<PrjApiTestDtl> findPrjApiTestDtl(PrjApiTestDtl prjApiTestDtl);
	
	public abstract int findPrjApiTestDtlCount(PrjApiTestDtl prjApiTestDtl);

	public abstract List<PrjApiTestDtl> findByPatId(@Param("patId")Integer patId);

	public abstract void updateStatusByPatId(@Param("patId")Integer patId, @Param("status")Integer status);

	public abstract void updateStatus(@Param("patId")Integer patId, @Param("path")String path,
			@Param("status")Integer status, @Param("testResult")String testResult);
}