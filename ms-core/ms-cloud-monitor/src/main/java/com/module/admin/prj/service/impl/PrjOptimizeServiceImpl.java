package com.module.admin.prj.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.module.admin.prj.dao.PrjOptimizeDao;
import com.module.admin.prj.pojo.PrjOptimize;
import com.module.admin.prj.service.PrjInfoService;
import com.module.admin.prj.service.PrjOptimizeService;
import com.system.comm.model.Page;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_optimize的Service
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
@Component
public class PrjOptimizeServiceImpl implements PrjOptimizeService {

	@Autowired
	private PrjOptimizeDao prjOptimizeDao;
	@Autowired
	private PrjInfoService prjInfoService;
	
	@Override
	public ResponseFrame saveOrUpdate(PrjOptimize prjOptimize) {
		ResponseFrame frame = new ResponseFrame();
		if(prjOptimize.getId() == null) {
			prjOptimizeDao.save(prjOptimize);
		} else {
			prjOptimizeDao.update(prjOptimize);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public PrjOptimize get(Integer id) {
		return prjOptimizeDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(PrjOptimize prjOptimize) {
		prjOptimize.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = prjOptimizeDao.findPrjOptimizeCount(prjOptimize);
		List<PrjOptimize> data = null;
		if(total > 0) {
			data = prjOptimizeDao.findPrjOptimize(prjOptimize);
			for (PrjOptimize po : data) {
				po.setPrjName(prjInfoService.getName(po.getPrjId()));
			}
		}
		Page<PrjOptimize> page = new Page<PrjOptimize>(prjOptimize.getPage(), prjOptimize.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(Integer id) {
		ResponseFrame frame = new ResponseFrame();
		prjOptimizeDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public PrjOptimize getByPrjIdUrl(Integer prjId, String url) {
		return prjOptimizeDao.getByPrjIdUrl(prjId, url);
	}
}