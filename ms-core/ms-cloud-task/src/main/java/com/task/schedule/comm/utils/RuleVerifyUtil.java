package com.task.schedule.comm.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameMapUtil;
import com.system.comm.utils.FrameStringUtil;

/**
 * 校验规则
 * @author  yuejing
 * @email   yuejing0129@163.com 
 * @net		www.suyunyou.com
 * @date    2015年4月3日 下午5:17:29 
 * @version 1.0.0
 */	
public class RuleVerifyUtil {

	/**
	 * 根据Http请求返回内容判断是否需要发送邮件
	 * @param ailemailrule
	 * @param content
	 * @return
	 */
	public static boolean isHttpResultSendMail(String failemailrule, String content) {
		if(FrameStringUtil.isEmpty(content) || FrameStringUtil.isEmpty(failemailrule)) {
			// 返回内容为空和失败通知规则为空都不发邮件
			return false;
		}
		if (!failemailrule.contains("=")) {
			// 处理返回内容相等的情况
			List<String> rules = FrameStringUtil.toArray(failemailrule, ";");
			for (String rule : rules) {
				if (rule.equalsIgnoreCase(content)) {
					return true;
				}
			}
			return false;
		} else {
			// 处理表达式的情况
			Map<String, Object> contMap = new HashMap<String, Object>();
			try {
				contMap = FrameJsonUtil.toMap(content);
			} catch (Exception e) {
			}
			if (failemailrule.contains("!=")) {
				// 判断key不等于的情况
				List<String> rules = FrameStringUtil.toArray(failemailrule, "!=");
				if (rules.size() < 2) {
					return false;
				}
				String key = rules.get(0);
				String value = rules.get(1);
				String resValue = FrameMapUtil.getString(contMap, key);
				if (!value.equalsIgnoreCase(resValue)) {
					return true;
				}
				return false;
			} else if (failemailrule.contains("=")) {
				// 判断key不等于的情况
				List<String> rules = FrameStringUtil.toArray(failemailrule, "=");
				if (rules.size() < 2) {
					return false;
				}
				String key = rules.get(0);
				String value = rules.get(1);
				String resValue = FrameMapUtil.getString(contMap, key);
				if (value.equalsIgnoreCase(resValue)) {
					return true;
				}
				return false;
			}
		}
		return false;
	}
	
	/*public static void main(String[] args) {
		String str = "{isSendMail:\"true\",mailTitle:\"邮件标题\",mailContent:\"邮件正文\",object:{id:1}}";
		System.out.println(isHttpResultSendMail(str));
	}*/
}